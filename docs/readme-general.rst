.. FastQC Workflow

===================================
Run FastQC in a General Environment
===================================

These instructions are specific for running FastQC in a general environment. Prior to running this workflow, please follow the GeneFlow setup and usage instructions here: https://git.biotech.cdc.gov/scbs/geneflow.

If you are in the CDC SciComp environment, you may prefer to run the workflow using the existing GeneFlow installation. If so, please refer to the `CDC Instructions` for running this workflow.

.. _CDC Instructions: readme-cdc.rst

Prepare Your Working Environment
================================

After installing GeneFlow in a Python virtual environment (see the GeneFlow general setup instructions) activate the environment with:

    ::

        source ~/geneflow_work/gfpy/bin/activate

    Note: these instructions assume that the installation working directory is ``~/geneflow_work``.


Install and Run the Workflow Locally
====================================

Follow these instructions to install and run the FastQC workflow locally.

1. Install the workflow:

    ::

        cd ~/geneflow_work
        geneflow install-workflow ./fastqc-gf -g git@git.biotech.cdc.gov:geneflow-workflows/fastqc-gf

    This command downloads the workflow specified at the git repo URL and installs it into the "./fastqc-gf" directory.

2. Copy test data to your working directory:

    ::

        cp -R ./fastqc-gf/data fastqc-reads

3. Run the workflow:

    ::

        geneflow run fastqc-gf -d name="Test local FastQC" -d output_uri=output -d inputs.files=fastqc-reads

4. The output of the workflow can be found in a sub-directory in ~/geneflow_work/output.

Install and Run the Workflow in Agave
=====================================

Follow these instructions to install and run the workflow in a general Agave environment. As a prerequisite, please be sure to follow the Agave-specific instructions for GeneFlow setup and usage here: https://git.biotech.cdc.gov/scbs/geneflow. Noteably, be sure to create your Agave token and create the agave-params.yaml file. 

1. Install the workflow:

    ::

        cd ~/geneflow_work
        geneflow install-workflow ./fastqc-gf -g git@git.biotech.cdc.gov:geneflow-workflow/fastqc-gf --agave_params ./agave-params.yaml

2. Copy test data to your working directory:

    ::

        cp -R ./fastqc-gf/data fastqc-reads

3. Run the workflow:

    ::

        geneflow run fastqc-gf -d name="Test FastQC in Agave" -d output_uri=output -d inputs.files=fastqc-reads -d work_uri.agave=agave://cobra-default-public-storage/[USER]/geneflow-work -d execution.context.default=agave

    Note that the "work_uri.agave" and "execution.context.default" parameters are set. Also be sure to replace [USER] with your CDC username.

4. The output of the workflow can be found in a sub-directory in ~/geneflow_work/output.


