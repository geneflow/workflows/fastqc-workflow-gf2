.. FastQC Workflow

=================================
Run FastQC in the CDC Environment
=================================

These instructions are specific for running the FastQC workflow in the CDC SciComp environment. Prior to running this workflow, please follow the GeneFlow setup and usage instructions here: https://git.biotech.cdc.gov/scbs/geneflow.

If you are not in the CDC SciComp environment, please refer to the `General Instructions`_ for running this workflow.

.. _General Instructions: readme-general.rst

Prepare Your Working Environment
================================

Prepare your environment by following these instructions. Note that you may have already done this if you followed the GeneFlow setup and usage instructions. 

1. Create and change to your working directory:

    ::

        mkdir ~/geneflow_work
        cd ~/geneflow_work

2. Create an output directory for workflow results. Each time you run the workflow and point the output_uri to this directory, a sub-directory will be created with new results.

    ::

        mkdir output

3. Load the GeneFlow module (note: this only works in the CDC SciComp environment):

    ::

        module load geneflow/latest

Run the Pre-Installed Workflow
==============================

Follow these instructions to run the pre-installed FastQC workflow.

1. Inside your working directory, copy sample data from the pre-installed workflow package:

    ::

        cd ~/geneflow_work
        cp -R /apps/geneflow/workflows/fastqc/0.1/data fastqc-reads

2. Run the workflow:

    ::

        geneflow run fastqc/0.1 -d name="Test pre-installed FastQC" -d output_uri=output -d inputs.files=fastqc-reads

3. The output of the workflow can be found in a sub-directory in ~/geneflow_work/output.

For additional information about the pre-installed FastQC workflow:

    ::

        geneflow help fastqc/0.1

Install and Run the Workflow Locally
====================================

Follow these instructions to install and run the FastQC workflow locally.

1. Install the workflow:

    ::

        cd ~/geneflow_work
        geneflow install-workflow ./fastqc-gf -g git@git.biotech.cdc.gov:geneflow-workflows/fastqc-gf

    This command downloads the workflow specified at the git repo URL and installs it into the "./fastqc-gf" directory.

2. Copy test data to your working directory:

    ::

        cp -R ./fastqc-gf/data fastqc-reads

3. Run the workflow:

    ::

        geneflow run fastqc-gf -d name="Test local FastQC" -d output_uri=output -d inputs.files=fastqc-reads

4. The output of the workflow can be found in a sub-directory in ~/geneflow_work/output.

Run the Pre-Installed Workflow in Agave
=======================================

Follow these instructions to run the pre-installed workflow in the CDC SciComp Agave environment. As a prerequisite, please be sure to follow the Agave-specific instructions for GeneFlow setup and usage here: https://git.biotech.cdc.gov/scbs/geneflow. Notably, be sure to create your Agave token. 

1. Inside your working directory, copy sample data from the pre-installed workflow package:

    ::

        cd ~/geneflow_work
        cp -R /apps/geneflow/workflows/fastqc/0.1/data fastqc-reads

2. Run the workflow:

    ::

        geneflow run fastqc/0.1 -d name="Test pre-installed FastQC in Agave" -d output_uri=output -d inputs.files=fastqc-reads -d work_uri.agave=agave://cobra-default-public-storage/[USER]/geneflow-work -d execution.context.default=agave

    Note that the "work_uri.agave" and "execution.context.default" parameters are set. Also be sure to replace [USER] with your CDC username.

3. The output of the workflow can be found in a sub-directory in ~/geneflow_work/output.

Install and Run the Workflow in Agave
=====================================

Follow these instructions to install and run the workflow in the CDC SciComp Agave environment. As a prerequisite, please be sure to follow the Agave-specific instructions for GeneFlow setup and usage here: https://git.biotech.cdc.gov/scbs/geneflow. Noteably, be sure to create your Agave token and create the agave-params.yaml file. 

1. Install the workflow:

    ::

        cd ~/geneflow_work
        geneflow install-workflow ./fastqc-gf -g git@git.biotech.cdc.gov:geneflow-workflow/fastqc-gf --agave_params ./agave-params.yaml

2. Copy test data to your working directory:

    ::

        cp -R ./fastqc-gf/data fastqc-reads

3. Run the workflow:

    ::

        geneflow run fastqc-gf -d name="Test FastQC in Agave" -d output_uri=output -d inputs.files=fastqc-reads -d work_uri.agave=agave://cobra-default-public-storage/[USER]/geneflow-work -d execution.context.default=agave

    Note that the "work_uri.agave" and "execution.context.default" parameters are set. Also be sure to replace [USER] with your CDC username.

4. The output of the workflow can be found in a sub-directory in ~/geneflow_work/output.

