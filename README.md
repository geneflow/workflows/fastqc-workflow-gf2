

# fastqc geneflow2 workflow

A workflow to run fastqc with geneflow v2

Inputs
------
1. FASTQ file(s)/directory

Outputs
-------
1. a html summary page

Apps Documentation
------------------
Documentation for the individual apps are in their respective directories:

|    https://gitlab.com/geneflow/apps/fastqc-gf2.git

